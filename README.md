<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# 1ª Instalar dependências do projeto:

Entre na pasta do projeto e execute<br>
A versão da sua máquina do node deve ser >= a v14.15.4 e a de seu composer >= v2.0.9 <br>

composer install<br>
npm install ou yarn<br>

# 2ª Configurar variáveis de ambiente:

cp .env.example .env <br>
php artisan key:generate<br>
<br>
Crie um schema no mysql com nome 'fpftech' (com o nome sem as aspas)
</br>
Insira suas credenciais do banco de dado no arquivo .env nos campos: <br>
DB_USERNAME= <br>
DB_PASSWORD= <br>

# 3ª Criar migrations (tabelas e Seeders):

php artisan migrate:fresh --seed

# 4ª Inicia o servidor:

php artisan serve

# 5ª Login do administrador do sistema:

Email: admin@fpftech.com <br>
Password: admin@1234 <br>
