<?php

namespace App\Http\Controllers;

use App\Models\Participant;
use App\Models\Project;
use App\Models\ProjectHasParticipants;
use DateTime;
use Illuminate\Http\Request;
use Yajra\DataTables\Contracts\DataTable;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::with('participants')->get();
        return view('dashboard', compact('projects'));
    }

    public function create()
    {
        $participants = Participant::all();
        return view('project.create', compact('participants'));
    }
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'price' => 'required|numeric',
            'risk' => 'required',
        ]);

        $project = Project::create([
            'name' => $request->name,
            'start_date' => DateTime::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d'),
            'end_date' => DateTime::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d'),
            'price' => floatval($request->price),
            'risk' => $request->risk,
        ]);

        $project->participants()->attach($request->participants);

        return redirect()->route('dashboard');
    }

    public function edit(Project $id)
    {
        $participants = Participant::all();
        return view('project.edit', ['project' => $id, 'participants' => $participants]);
    }
    public function update(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'price' => 'required|numeric',
            'risk' => 'required',
        ]);

        $project = Project::find($request->id);

        $project->update([
            'name' => $request->name,
            'start_date' => DateTime::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d'),
            'end_date' => DateTime::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d'),
            'price' => floatval($request->price),
            'risk' => $request->risk,
        ]);

        $project->save();

        $project->participants()->detach();
        $project->participants()->attach($request->participants);

        return redirect()->route('dashboard');
    }
    public function destroy($id)
    {
        if ($id) {
            $record = Project::where('id', $id);
            $record->delete();
        }
        return redirect()->route('dashboard');
    }
}
