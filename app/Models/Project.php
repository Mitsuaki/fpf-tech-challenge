<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'price',
        'risk',
        'participant_id',
    ];

    public function participants()
    {
        return $this->belongsToMany(Participant::class, 'project_has_participants', 'project_id', 'participant_id');
    }
}
