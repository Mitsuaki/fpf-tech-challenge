<?php

namespace Database\Factories;

use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(),
            'start_date' => now(),
            'end_date' => $this->faker->dateTimeBetween($startDate = 'now', $endDate = '+2 years'),
            'price' => $this->faker->randomNumber(6),
            // 'price' => $this->faker->numberBetween(0, 2),
            'risk' => $this->faker->randomElement(['0', '1', '2']),
            'created_at' => now()
        ];
    }
}
