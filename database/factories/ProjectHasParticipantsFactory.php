<?php

namespace Database\Factories;

use App\Models\ProjectHasParticipants;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectHasParticipantsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProjectHasParticipants::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'project_id' => $this->faker->numberBetween(1, 5),
            'participant_id' => $this->faker->numberBetween(1, 5),
            'created_at' => now()
        ];
    }
}
