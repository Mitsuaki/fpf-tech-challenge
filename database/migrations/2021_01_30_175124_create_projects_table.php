<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->longText('name');
            $table->date('start_date');
            $table->date('end_date');
            $table->double('price', 15, 2);
            $table->enum('risk', [0, 1, 2]);
            // $table->foreignId('participant_id')->nullable();
            // $table->foreignId('current_team_id')->nullable();
            // $table->foreign('participant_id')->references('id')->on('participants');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
