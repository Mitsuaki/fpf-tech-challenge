<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectHasParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_has_participants', function (Blueprint $table) {
            $table->id();
            // $table->unsignedBigInteger('project_id');
            $table->foreignId('project_id')->references('id')->on('projects')->onDelete('cascade');
            // $table->unsignedBigInteger('participant_id');
            $table->foreignId('participant_id')->references('id')->on('participants')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_has_participants');
    }
}
