<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProjectHasParticipants;

class ProjectHasParticipantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectHasParticipants::factory()
            ->count(5)
            ->create();
    }
}
