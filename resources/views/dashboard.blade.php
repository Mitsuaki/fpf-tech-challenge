<x-app-layout>
    <x-slot name="header">
        <div class="row">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{-- {{ __('Dashboard') }} --}}
            Projetos
        </h2>
        <div class="font-semibold text-xl text-gray-800 leading-tight" style="margin-left: 70%">
            {{-- <button type="button" class="btn btn-dark">Cadastrar</button> --}}
            <a class="btn btn-dark" href="{{route('project.create')}}" role="button"><i class="bi bi-plus-square"></i> Cadastrar</a>
        </div></div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:20px;">
                <table id="table-project" class="table table-striped table-bordered" style=" width:100%">
                {{-- <table class="table table-striped"> --}}
                    <thead>
                      <tr>
                        <th scope="col" style="vertical-align: middle !important">ID</th>
                        <th scope="col" style="vertical-align: middle !important">Nome</th>
                        <th scope="col" style="vertical-align: middle !important">Data de Início</th>
                        <th scope="col" style="vertical-align: middle !important">Data de Término</th>
                        <th scope="col" style="vertical-align: middle !important">Valor do Projeto</th>
                        <th scope="col" style="vertical-align: middle !important">Risco</th>
                        <th scope="col" style="vertical-align: middle !important">Participantes</th>
                        <th scope="col" style="vertical-align: middle !important">Ações</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($projects as $project)
                        <tr>
                            <th scope="row" style="vertical-align: middle !important"> {{$project->id}}</th>
                            <td style="vertical-align: middle !important">{{$project->name}}</td>
                            <td style="vertical-align: middle !important">{{date('d/m/Y', strtotime($project->start_date))}}</td>
                            <td style="vertical-align: middle !important">{{date('d/m/Y', strtotime($project->end_date))}}</td>
                            <td style="vertical-align: middle !important">R${{number_format($project->price,2,",",".")}}</td>
                            <td style="vertical-align: middle !important">
                                @if ($project->risk == 0)
                                <span class="badge badge-success">Baixo</span>
                                @elseif($project->risk == 1)
                                <span class="badge badge-warning">Médio</span>
                                @elseif($project->risk == 2)
                                <span class="badge badge-danger">Alto</span>
                                @endif
                            </td>
                            <td style="vertical-align: middle !important">
                                @foreach ($project->participants as $participant)
                                <span>{{$participant->name}}</span>
                                @endforeach
                            </td>
                            <td style="vertical-align: middle !important">
                                <span>
                                        <a class="btn btn-secondary btn-sm" style="width:100px; margin:2px" href="{{route('project.edit',$project->id)}}" role="button">
                                            <div style="vertical-align: middle">
                                                <i class="bi bi-pencil-fill"></i> Editar
                                            </div>
                                        </a>
                                </span>
                                <span>
                                        {{-- <a class="btn btn-info btn-sm" style="width:100px; margin:2px" href="{{route('project.destroy',$project->id)}}" role="button">
                                            <div>
                                                <i class="bi bi-cash-stack"></i> Simular <br> Investimento
                                            </div>
                                        </a> --}}
                                        <button type="button" class="btn btn-info btn-sm" style="width:100px; margin:2px" onclick="calcInvest({{$project->risk}},{{$project->price}})" data-toggle="modal" data-target="#exampleModalCenter">
                                            <i class="bi bi-cash-stack"></i> Simular <br> Investimento
                                        </button>
                                </span>
                                <span>
                                    <a class="btn btn-danger btn-sm" style="width:100px; margin:2px" onclick="return confirm('Tem certeza que desejar excluir este registro?');" href="{{route('project.destroy',$project->id)}}" role="button">
                                        <div>
                                            <i class="bi bi-trash" ></i> Excluir
                                        </div>
                                    </a>
                            </span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</x-app-layout>
<script type="text/javascript">
    function calcInvest(risk, price) {

        var investimento = prompt("Informe o total de investimento:");
        if(investimento<price){
            alert("O valor de investimento não pode ser menor que o valor do projeto.");
        }else{
            if(risk == 0){
            alert("Para o investimento de R$"+parseFloat(investimento).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})+" e o risco baixo o valor de retorno será de R$"+(investimento*0.05).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}))
        }else if(risk == 1){
            alert("Para o investimento de R$"+parseFloat(investimento).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})+" e o risco médio o valor de retorno será de R$"+(investimento*0.10).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}))
        }else if(risk == 2){
            alert("Para o investimento de R$"+parseFloat(investimento).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})+" e o risco alto o valor de retorno será de R$"+(investimento*0.20).toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}))
        }
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#table-project').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registos por página",
                "zeroRecords": "Nada foi encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Não há registos disponíveis",
                "infoFiltered": "(filtrado de _MAX_ registos totais)",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                },
                "select": {
                    "rows": {
                        "0": "Nenhuma linha selecionada",
                        "1": "Selecionado 1 linha",
                        "_": "Selecionado %d linhas"
                        }
                },
                "buttons": {
                    "copy": "Copiar para a área de transferência",
                    "copyTitle": "Cópia bem sucedida",
                    "copySuccess": {
                        "1": "Uma linha copiada com sucesso",
                        "_": "%d linhas copiadas com sucesso"
                    }
                }
            }
        });
    });
</script>
