
<x-app-layout>
    <x-slot name="header">
        <div class="row">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{-- {{ __('Dashboard') }} --}}
                Projetos
            </h2>
            <div class="font-semibold text-xl text-gray-800 leading-tight" style="margin-left: 70%">
                {{-- <button type="button" class="btn btn-dark">Cadastrar</button> --}}
                <a class="btn btn-dark" href="{{route('dashboard')}}" role="button"><i class="bi bi-arrow-left"></i> Voltar </a>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding:20px;">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('project.store')}}" method="POST">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="nameProject">Nome do Projeto</label>
                        <input type="text" class="form-control" id="name-project" name="name" placeholder="Informe aqui o nome do projeto" value="" required>
                    </div>

                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="startDate"> Data de Início </label>
                        <div id="start_date" class="input-group date" data-date-format="dd/mm/yyyy">
                            <input class="form-control" type="text" name="start_date" value="{{date("d/m/Y")}}" required/>
                            <span class="input-group-addon">
                                <div class="input-group-text">
                                    <i class="bi bi-calendar-event"></i>
                                </div>
                            </span>
                        </div>
                    </div>
                      <div class="form-group col-md-6">
                        <label for="endDate"> Data de Término </label>
                        {{-- <input type="date" class="form-control" id="end_date" name="end_date" placeholder="Informe aqui a data de término" value="" required> --}}
                        <div id="end_date" class="input-group date" data-date-format="dd/mm/yyyy" >
                            <input class="form-control" type="text" name="end_date" value="{{date("d/m/Y")}}"required/>
                            <span class="input-group-addon">
                                <div class="input-group-text">
                                    <i class="bi bi-calendar-event"></i>
                                </div>
                            </span>
                        </div>
                    </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="price"> Valor do Projeto </label>
                          <input type="text" class="form-control" id="price" name="price" placeholder="Informe aqui o valor do projeto" value="" required>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="risk"> Risco </label>
                          <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="risk" required>
                                <option value = "0" selected>Baixo</option>
                                <option value = "1">Médio</option>
                                <option value = "2">Alto</option>
                          </select>
                          {{-- <input type="text" class="form-control" id="risk" name="risk" placeholder="Informe aqui o risco do projeto" value="{{$project->risk}}" required> --}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nameProject">Participantes</label>
                        <select class="custom-select" name="participants[]" multiple required>
                            @foreach ($participants as $participant)
                                <option value="{{$participant->id}}">{{$participant->name}}</option>
                            @endforeach
                          </select>
                    </div>

                    <button type="submit" class="btn btn-success">Salvar</button>
                  </form>
            </div>
        </div>
    </div>
</x-app-layout>

<script type="text/javascript">
    $(function () {
        $("#start_date").datepicker({
                autoclose: true,
                todayHighlight: true
        }).datepicker('update', new Date());
    });
    $(function () {
        $("#end_date").datepicker({
            autoclose: true,
            todayHighlight: true
    }).datepicker('update', new Date());
    });


</script>
