<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', 'App\Http\Controllers\ProjectController@index')->name('dashboard');

Route::get('/projects-delete/{id}', 'App\Http\Controllers\ProjectController@destroy')->name('project.destroy');
Route::get('/projects-edit/{id}', 'App\Http\Controllers\ProjectController@edit')->name('project.edit');
Route::post('/projects-update', 'App\Http\Controllers\ProjectController@update')->name('project.update');

Route::get('/projects-create', 'App\Http\Controllers\ProjectController@create')->name('project.create');
Route::post('/projects-store', 'App\Http\Controllers\ProjectController@store')->name('project.store');


// Route::get('/reservista-delete/{id}', 'ReservistaController@deletar')->name('delete');
